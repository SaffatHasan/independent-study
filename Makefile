DEPENDS_JAR=$(CURDIR)/depends-0.9.3/depends.jar
DEPENDS=exec java -jar $(DEPENDS_JAR)
DEPENDS_ARGS=-m=true -s -p=dot --auto-include
LANG=java

SOURCE_CODE=surveytest
DEPENDS_OUTPUT_DIR=surveytest/DependsOutput

PROJECT_NAME=surveytest

DV8_CONSOLE_ARGS=arch-report
DV8_CONSOLE=/Applications/DV8/bin/dv8-console
DSM_OUTPUT_FILE=$(DEPENDS_OUTPUT_DIR)/$(PROJECT_NAME).dv8-dsm

DEPENDS_VERSION=0.9.3
DEPENDS_TAR_FILE=$(DEPENDS_VERSION).tar.gz
DEPENDS_LINK=https://github.com/multilang-depends/depends/releases/download/0.9.4/$(DEPENDS_TAR_FILE)
DEPENDS_FOLDER=depends-$(DEPENDS_VERSION)

depends: $(DEPENDS_FOLDER)

$(DEPENDS_FOLDER):
	wget $(DEPENDS_LINK)
	tar -xzf $(DEPENDS_TAR_FILE) 
	rm $(DEPENDS_TAR_FILE)

setup:
	mkdir -p $(DEPENDS_OUTPUT_DIR)

generate: setup
	$(DEPENDS) $(LANG) $(SOURCE_CODE) $(PROJECT_NAME)-json-dep $(DEPENDS_ARGS) -d=$(DEPENDS_OUTPUT_DIR)

convert:
	# Convert from json to dv8-dsm
	$(DV8_CONSOLE) core:convert-matrix $(DEPENDS_OUTPUT_DIR)/$(PROJECT_NAME)-json-dep.json -outputFile $(DSM_OUTPUT_FILE)

decoupling-level:
	$(DV8_CONSOLE) metrics:decoupling-level $(DSM_OUTPUT_FILE)

clean:
	rm -rf OutputData/
